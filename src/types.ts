type TableDataType = {
  data: { [key: string]: string };
};

export type TableRowType = TableDataType & {
  children: TableRowType[];
};

export type ExampleDataRowType = TableDataType & {
  children: {
    [key: string]: { records: ExampleDataRowType[] };
  };
};
