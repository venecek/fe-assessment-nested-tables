import React from 'react';
import TableContainer from './components/TableContainer';

function App() {
  return (
    <div className="bg-gray-500 min-h-screen">
      <div className="mx-auto">
        <TableContainer />
      </div>
    </div>
  );
}

export default App;
