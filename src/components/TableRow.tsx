import React, { useState } from 'react';
import { TableRowType } from '../types';
import Table from './Table';
import IconChevron from './icons/IconChevron';
import IconXMark from './icons/IconXMark';
import { useTableStore } from '../store/data';

interface TableRowProps {
  row: TableRowType;
  path: string[];
}

const TableRow: React.FC<TableRowProps> = ({ row, path }) => {
  const [isOpen, setIsOpen] = useState(false);
  const deleteData = useTableStore((store) => store.deleteData);
  const hasChildren = !!row.children.length;

  return (
    <>
      <tr className="bg-white border-b dark:bg-gray-800 dark:border-gray-700">
        {/* Row expansion. */}
        <td>
          {hasChildren && (
            <button className="p-4" onClick={() => setIsOpen((prev) => !prev)}>
              <IconChevron
                className={`w-6 h-6 transition-transform duration-300 ${isOpen && 'transform rotate-180'}`}
              />
            </button>
          )}
        </td>
        {/* Row values. */}
        {Object.values(row.data).map((val) => (
          <td className="px-6 py-4" key={val}>
            {val}
          </td>
        ))}
        {/* Row deletion. */}
        <td>
          <button
            className="p-4"
            onClick={() => {
              deleteData(path);
            }}
          >
            <IconXMark />
          </button>
        </td>
      </tr>
      {/* Row children (nested tables). */}
      {isOpen && hasChildren && (
        <tr>
          <td colSpan={100} className="p-0">
            <div className="ml-4 max-w-min">
              <Table path={path} rows={row.children} />
            </div>
          </td>
        </tr>
      )}
    </>
  );
};

export default TableRow;
