import React from 'react';
import TableHeader from './TableHeader';
import TableRow from './TableRow';
import { TableRowType } from '../types';

interface TableProps {
  rows: TableRowType[];
  className?: string;
  path?: string[];
}

const Table: React.FC<TableProps> = ({ rows, path = [] }) => {
  const columns = rows.length ? Object.keys(rows[0].data) : [];

  return (
    <div className="relative overflow-x-auto bg-gray-500 whitespace-nowrap">
      <table className="max-w-min text-sm text-center text-gray-500 dark:text-gray-400">
        <TableHeader columns={columns} />
        <tbody>
          {rows.map((row) => {
            const nextPath = [...path, row.data['ID']];
            return <TableRow path={nextPath} key={nextPath.join('-')} row={row} />;
          })}
        </tbody>
      </table>
    </div>
  );
};

export default Table;
