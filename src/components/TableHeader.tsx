import React from 'react';
import IconTrash from './icons/IconTrash';

interface TableHeaderProps {
  columns: string[];
}

const TableHeader: React.FC<TableHeaderProps> = ({ columns }: TableHeaderProps) => {
  return (
    <thead className="text-xs text-gray-700 uppercase bg-gray-50 dark:bg-gray-700 dark:text-gray-400">
      <tr>
        {/* Empty table header for the row expansion button. */}
        <th></th>
        {columns.map((col) => (
          <th scope="col" className="px-6 py-3" key={col}>
            {col}
          </th>
        ))}
        <th className="flex items-center justify-center px-6 py-3">
          <IconTrash />
        </th>
      </tr>
    </thead>
  );
};

export default TableHeader;
