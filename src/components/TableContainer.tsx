import React, { useEffect } from 'react';
import Table from '../components/Table';
import { useTableStore } from '../store/data';

const TableContainer = () => {
  const initializeData = useTableStore((state) => state.initializeData);
  const tableData = useTableStore((state) => state.data);

  useEffect(() => {
    initializeData();
  }, []);

  return tableData.length ? (
    <Table rows={tableData} />
  ) : (
    <div className="text-md font-bold p-4">No data, reload the page.</div>
  );
};

export default TableContainer;
