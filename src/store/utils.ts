import { TableRowType, ExampleDataRowType } from '../types';

export const formatRow = (row: ExampleDataRowType): TableRowType => {
  const keys = Object.keys(row.children);
  return {
    ...row,
    children: keys.length ? row.children[keys[0]]['records'].map((record) => formatRow(record)) : [],
  };
};

export const deleteRow = (rows: TableRowType[], deletionPath: string[]): TableRowType[] => {
  const [currentId, ...otherIds] = deletionPath;
  if (!otherIds.length) {
    return rows.filter((row) => row.data['ID'] !== currentId);
  }
  return rows.map((row) => {
    if (row.data['ID'] === currentId) {
      return { ...row, children: deleteRow(row.children, otherIds) };
    }
    return row;
  });
};
