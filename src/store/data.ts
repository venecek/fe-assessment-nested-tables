import create from 'zustand';
import exampleData from '../assets/example-data.json';
import { TableRowType, ExampleDataRowType } from '../types';
import { formatRow, deleteRow } from './utils';

interface TableState {
  data: TableRowType[];
  initializeData: () => void;
  deleteData: (deletionPath: string[]) => void;
}

export const useTableStore = create<TableState>()((set) => ({
  data: [],
  initializeData: () => {
    const data = exampleData as unknown as ExampleDataRowType[];
    set({ data: data.map((row) => formatRow(row)) });
  },
  deleteData: (deletionPath) =>
    set((state) => {
      return { data: deleteRow(state.data, deletionPath) };
    }),
}));
