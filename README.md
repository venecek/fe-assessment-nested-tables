## FE Technical Assessment

### Running the app

```bash
npm install
npm run dev
```

### Used technologies

- React.JS
- Typescript
- Tailwind CSS
- Zustand
- Vite
- Prettier
- ESLint
